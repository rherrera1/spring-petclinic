FROM openjdk:latest

WORKDIR /app
COPY target/*.jar /app

ENTRYPOINT ["java", "-jar", "/app/spring-petclinic-2.2.0.BUILD-SNAPSHOT.jar"]
